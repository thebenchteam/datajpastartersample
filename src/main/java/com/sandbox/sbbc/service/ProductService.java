package com.sandbox.sbbc.service;

import com.sandbox.sbbc.entity.Product;
import com.sandbox.sbbc.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public void generateProducts() {
        Product aProduct = new Product();
        aProduct.setDescription("A Product");
        aProduct.setPrice(new BigDecimal("13.18"));
        productRepository.save(aProduct);
    }

    public List<Product> retrieveProducts() {
        return productRepository.findAll();
    }
}
